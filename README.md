# Guide for PLTOffline 
This guide is intended to setup the environment for working with the PLTOffline repository. It also contains an example on how to run `OccupancyPlots` for a specific fill. 

## Prerequisites 
In order to run the code below you have to be ***registered*** as a user to the PLTOffline PC and have your own user area. 

## Before we begin
The process of setting the environment is automated by `setup_pltoffline.sh`. 
This script: 
1. Installs python3 from Brilconda3
2. Installs/Upgrades python packages
3. Generates the SSH keys needed for the GitHub repository
4. Clones the PLTOffline GitHub repository
5. Compiles the PLTOffline code

***IMPORTANT NOTE!***  
_Carefully read all the messages that the script produces. It contains steps that you need to do manually._

## Setting the environment
After you ssh to the PLTOffline PC run the following sequence of commands:
1. `cd /home/$USER/`
2. `wget https://raw.githubusercontent.com/cmsplt/PLTOffline/master/setup_pltoffline.sh`  
It is _suggested_ to change the installation folder to `/home/$USER`. To do so:  
    * vim setup_pltoffline.sh
    * Replace the 8th line of the script to `wDir="/home/$USER"`  
    * Save and exit
3. `source setup_pltoffline.sh`  

***Reminder***  
_Carefully read all the messages that the script produces. It contains steps that you need to do manually._

You will need to provide your GitHub credentials and add the SSH keys generated to your GitHub account. 
After that the repository will be cloned to `/.local/venv/plt/PLTOffline`

***DONE!***

### Additional stuff
To ensure tha everything is set up correctly

* `cd /home/$USER/PLTOffline/`

* `git status`

If this message (or something similar) appears, the repository was cloned correctly

`# On branch master
nothing to commit, working directory clean`

You can check your ssh config file by: 
* `cat ~/.ssh/config `

You can check your gitconfig file by:
* `cat ~/.gitconfig`

### Make your life easier
In order to be able to write the beginning of a command and cycle through matches in your history you have to make an `.inputrc` config file:

1. `cd /home/$USER`
2. `vim ~/.inputrc`
3. Insert the text:  
 `"\e[A": history-search-backward`  
 `"\e[B": history-search-forward`
4. Save and exit     

_The changes will take effect on the next session._

Check more about this [here](https://codeinthehole.com/tips/the-most-important-command-line-tip-incremental-history-searching-with-inputrc/) .
## Running the code 
In this part we will run the `OccupancyPlots` executable file. Errors will be made ***intentionally*** to help you understand the basic principles of this repo. 

* `cd /home/$USER/PLTOffline`  
* `./OccupancyPlots`

You will get this error:  
`./OccupancyPlots: error while loading shared libraries: libCore.so: cannot open shared object file: No such file or directory`

To fix it run:   
* `source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.06.08/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh`

***Note***  
_Sourcing `thisroot.sh` is needed every time you run the code. So it's really helpful to append an alias in your `.bashrc` config file to avoid that. To do so:_  
* `echo "alias root6='source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.06.08/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh'" >> "${HOME}/.bashrc"`  

and now run again:  
* `./OccupancyPlots`
  
You will get this message:   
`Usage: ./OccupancyPlots [DataFile.dat]`  
which indicates that we have to specify a `DataFile.dat` path as an input to the executable.

### Finding the fill's timestamp
#### Method 1: The hard way

Let's assume that we want to execute the `./OccupancyPlots` for fill 4984. We need to find the timestamps of this fill. Let's visit[ CMS OMS](https://cmsoms.cern.ch/). 

1. Click on the top bar:
MWGRs -> Fills
![](photos/Screenshot_1.png/)  

2. Filter as shown below and hit Apply.  
![](photos/Screenshot_2.png/)  

3. Select the fill  
![](photos/Screenshot_3.png/)  
 
4. The timestamp information is provided below  
![](/photos/Screenshot_5.png/)   

We need the Stable Beams Time which is: `2016-06-03 15:36:42` 
We will need these timestamps to find the corresponding `DataFile.dat`

#### Method 2 : The easy way

`cd /home/$USER/PLTOffline/`

The 'pltTimestamps.csv' maps each fill with the Timestamps corresponding to that fill. 
Search for fill 4984:
1. `grep 4984 pltTimestamps.csv `
Print the column names:
2.  `head -1 pltTimestamps.csv`

The output of the above commands is:   
`4984|20160603.104035|20160603.153642|20160603.155754|20160603.160804|20160603.120829 20160603.145749 20160603.151137|20160603.153652|20160819.113115|Trans_Alignment_4892.dat|TrackDistributions_MagnetOn2016_4892.txt`  
`fill|start_time|start_stable_beam|end_stable_beam|end_time|wloopTS|slinkTS|gainCal|alignment|trackDist`

We need the slinkTS timestamp which is: `20160603.153652` 


### Specify the DataFile.dat
All the DAQ files are placed in `/localdata/`
1. `cd /localdata/`  
If we `ls` in the folder we can see that the data is organized by year. Our timestamp corresponds to 2016.  
2. `cd 2016`  
If we `ls` in the folder we see:  
`CALIB  CONF  SLINK  WORKLOOP`
The data we need is placed in the SLINK folder.  
3. `cd SLINK`
Methods 1 & 2 gave us the timestamp we needed. So we search for the corresponding `DataFile.dat` 
4. `ls -l Slink_20160603.15*`  

Two file exist with this timestamp.  
`-rw-r--r--. 1 adelanno zh  213372992 Jun  3  2016 Slink_20160603.153652.dat`  

`-rw-r--r--. 1 adelanno zh 1541483416 Jun  3  2016 Slink_20160603.155921.dat`

But the one closer to Stable Beam Time is the first one. So we choose that one.

We notice that the second method gives us the exact timestamp of the Slink file, so we don't need to search for it.   

So the full path for fill ***4984***  is  
`/localdata/2016/SLINK/Slink_20160603.153652.dat`

### Ok, there is a much easier way 
* `ll /localdata/2016/SLINK/Slink_$(awk 'BEGIN {FS="|"}; /^4984/{print $7}' /localdata/pltTimestamps.csv).dat`  

The output of the command should be:  
`-rw-r--r--. 1 adelanno zh 213372992 Jun  3  2016 /localdata/2016/SLINK/Slink_20160603.153652.dat`  
from which you can copy the path. 


## Running the OccupancyPlots executable and finding the plots
Now we can run the executable by: 
* `cd /home/$USER/PLTOffline/`  
* `./OccupancyPlots /localdata/2016/SLINK/Slink_20160603.153652.dat`  

All the plots produced are saved in:  
`/home/stsoukia/PLTOffline/plots`

You can see them with the command: 
* `eog Occupancy_All_Ch10.gif`  for example

***Note***
_To use the eog you have to SSH with the flag `-CXY`_



